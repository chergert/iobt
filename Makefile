all: libiobt.so

libiobt.so: iobt.c
	$(CC) -std=gnu11 -ggdb -o $@ -Wall -fPIC -shared -Wl,-z,now -Wl,-Bsymbolic -Wl,-z,defs -Wl,-z,relro -ldl iobt.c $(shell pkg-config --cflags --libs sysprof-3)

clean:
	rm -f libiobt.so
