#define _GNU_SOURCE

#include <dlfcn.h>
#include <execinfo.h>
#include <fcntl.h>
#include <glib-unix.h>
#include <sched.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sysprof.h>
#include <unistd.h>

#define MAX_BACKTRACE 256 /* 1 page */

static int (*real_open) (const char *filename, int flags, ...);
static int (*real_fsync) (int fd);
static int (*real_fdatasync) (int fd);
static int (*real_msync) (void *addr, size_t length, int flags);
static int (*real_syncfs) (int fd);
static void (*real_sync) (void);

static SysprofCaptureWriter *writer;
static gboolean loading;

static inline int
is_main_thread (void)
{
  return (pid_t)syscall (__NR_gettid, 0) == getpid ();
}

static void
suppliment_writer (void)
{
  SysprofCaptureReader *reader;
  SysprofSource *source;

  reader = sysprof_capture_writer_create_reader (writer, 0);
  source = sysprof_symbols_source_new ();
  sysprof_symbols_source_set_user_only (SYSPROF_SYMBOLS_SOURCE (source), TRUE);
  sysprof_source_set_writer (source, writer);
  sysprof_source_supplement (source, reader);
  g_object_unref (source);
  sysprof_capture_reader_unref (reader);
  sysprof_capture_writer_flush (writer);
  g_clear_pointer (&writer, sysprof_capture_writer_unref);
}

static void
load_proc_info (SysprofCaptureWriter *writer)
{
  gchar *maps = NULL;
  gchar **lines;
  gsize len;

  sysprof_capture_writer_add_process (writer,
                                      SYSPROF_CAPTURE_CURRENT_TIME,
                                      sched_getcpu (),
                                      getpid (),
                                      g_get_prgname ());

  /* NOTE: This does not track new loads via dlopen(), or mmap() of
   *       executable code.
   */

  if (!g_file_get_contents ("/proc/self/maps", &maps, &len, NULL))
    return;

  lines = g_strsplit (maps, "\n", 0);

  for (guint i = 0; lines[i]; i++)
    {
      gchar file[512];
      gulong start;
      gulong end;
      gulong offset;
      gulong inode;
      gint r;

      r = sscanf (lines[i],
                  "%lx-%lx %*15s %lx %*x:%*x %lu %512s",
                  &start, &end, &offset, &inode, file);
      file [sizeof file - 1] = '\0';

      if (r != 5)
        continue;

      if (strcmp ("[vdso]", file) == 0)
        {
          offset = 0;
          inode = 0;
        }

      sysprof_capture_writer_add_map (writer,
                                      SYSPROF_CAPTURE_CURRENT_TIME,
                                      sched_getcpu (),
                                      getpid (),
                                      start,
                                      end,
                                      offset,
                                      inode,
                                      file);
    }

  g_strfreev (lines);
  g_free (maps);
}

static gboolean
do_sigusr2 (gpointer data)
{
  exit (0);
  return FALSE;
}

static SysprofCaptureWriter *
get_capture (void)
{
  g_assert (is_main_thread ());

  if G_UNLIKELY (writer == NULL)
    {
      gchar *name = g_strdup_printf ("main-thread-io.%d.syscap", getpid ());

      loading = TRUE;
      writer = sysprof_capture_writer_new (name, 0);
      load_proc_info (writer);
      atexit (suppliment_writer);
      loading = FALSE;

      g_unix_signal_add (SIGUSR2, do_sigusr2, NULL);

      g_free (name);
    }

  return writer;
}

static void
do_backtrace (void)
{
  SysprofCaptureWriter *writer = get_capture ();
  void **addrs = alloca (sizeof (void*) * MAX_BACKTRACE);
  int n_addrs = backtrace (addrs, MAX_BACKTRACE);

#if GLIB_SIZEOF_VOID_P != 8
# error Only 64-bit is supported currently
#endif

  if (n_addrs > 0)
    sysprof_capture_writer_add_sample (writer,
                                       SYSPROF_CAPTURE_CURRENT_TIME,
                                       sched_getcpu (),
                                       getpid (),
                                       getpid (), /* always main thread */
                                       (SysprofCaptureAddress *)(gpointer)addrs,
                                       n_addrs);
}

static void
clear_preload (void)
{
  static gboolean did_clear;

  if (!did_clear)
    {
      g_unsetenv ("LD_PRELOAD");
      did_clear = TRUE;
    }
}

int
open (const char *filename, int flags, ...)
{
  mode_t mode;
  va_list args;

  clear_preload ();

  if (real_open == NULL)
    real_open = dlsym (RTLD_NEXT, "open");

  va_start (args, flags);
  mode = va_arg (args, mode_t);
  va_end (args);

  if (is_main_thread () && !loading)
    {
      do_backtrace ();
      sysprof_capture_writer_add_log (writer,
                                      SYSPROF_CAPTURE_CURRENT_TIME,
                                      sched_getcpu (),
                                      getpid (),
                                      G_LOG_LEVEL_DEBUG,
                                      "iobt::open()",
                                      filename);
    }

  return real_open (filename, flags, mode);
}

int
fsync (int fd)
{
  clear_preload ();

  if (real_fsync == NULL)
    real_fsync = dlsym (RTLD_NEXT, "fsync");

  if (is_main_thread () && !loading)
    do_backtrace ();

  return real_fsync (fd);
}

int
fdatasync (int fd)
{
  clear_preload ();

  if (real_fdatasync == NULL)
    real_fdatasync = dlsym (RTLD_NEXT, "fdatasync");

  if (is_main_thread () && !loading)
    do_backtrace ();

  return real_fdatasync (fd);
}

int
msync (void   *addr,
       size_t  length,
       int     flags)
{
  clear_preload ();

  if (real_msync == NULL)
    real_msync = dlsym (RTLD_NEXT, "msync");

  if (is_main_thread () && !loading)
    do_backtrace ();

  return real_msync (addr, length, flags);
}

int
syncfs (int fd)
{
  clear_preload ();

  if (real_syncfs == NULL)
    real_syncfs = dlsym (RTLD_NEXT, "syncfs");

  if (is_main_thread () && !loading)
    do_backtrace ();

  return real_syncfs (fd);
}

void
sync (void)
{
  clear_preload ();

  if (real_sync == NULL)
    real_sync = dlsym (RTLD_NEXT, "sync");

  if (is_main_thread () && !loading)
    do_backtrace ();

  real_sync ();
}
