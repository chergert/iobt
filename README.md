# IOBT

Note that this currently requires Sysprof from master to be able to view the files properly due to a fix related to context switch tracking.

This gets backtrace() for various calls you might not want to be doing on your main thread.
Right now, it stashes a backtrace for:

 - open()
 - fsync()
 - fdatasync()
 - msync()
 - syncfs()
 - sync()

if they are called on the main thread.

A `main-thread-io.$pid.syscap` file is generated with information about the backtrace.

If you cannot cleanly exit your program (like GNOME Shell), then you can send `kill -SIGUSR2 $pid` to force exit after flushing the capture buffers and decoding symbols.

Open the file with Sysprof and you'll see something like:

![Screenshot showing callgraph](https://gitlab.gnome.org/chergert/iobt/raw/master/screenshot1.png)
